package ru.urfu.autoaccessors.annotations;

import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

import ru.urfu.autoaccessors.annotations.pojo.PojoAnnotationToFieldsMapper;

public abstract class AnnotationToFieldsMapperBase {

	public abstract Set<VariableElement> map(Set<? extends Element> annotatedElements);

	public static AnnotationToFieldsMapperBase createFor (TypeElement annotationType) {
		
		if (annotationType.toString().contains("Pojo"))
			return new PojoAnnotationToFieldsMapper();
		
		return new DefaultAnnotationToFieldsMapper();
	}

	protected AnnotationToFieldsMapperBase() {
	}
}