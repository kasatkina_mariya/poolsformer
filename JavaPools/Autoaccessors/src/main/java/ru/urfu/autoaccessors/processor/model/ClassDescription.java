package ru.urfu.autoaccessors.processor.model;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;

import freemarker.template.DefaultObjectWrapper;
import ru.urfu.autoaccessors.processor.Freemarker;
import ru.urfu.autoaccessors.processor.Utils;

public class ClassDescription extends DefaultObjectWrapper {

	public String simpleName;
	public Modifier accessModifier;
	public String fullPackageName;

	public ClassDescription(TypeElement type) {

		super(Freemarker.VERSION);
		
		simpleName = type.getSimpleName().toString();
		fullPackageName = ((PackageElement)type.getEnclosingElement())
						  .getQualifiedName().toString();
		accessModifier = Utils.extractAccessModifier(type.getModifiers());
	}

	@Override
	public int hashCode() {
		return simpleName.hashCode();
	}

	@Override
	public boolean equals(Object obj) {

    	if (null == obj)
    		return false;
    	if (this == obj)
    		return true;    	    	
    	if (!(obj instanceof ClassDescription))
    		return false;

		ClassDescription another = (ClassDescription) obj;
		return this.fullPackageName.equals(another.fullPackageName)
			&& this.simpleName.equals(another.simpleName);
	}

	@Override
	public String toString() {
		return String.format("n='%s' p='%s' m='%s'",
							  simpleName, fullPackageName, accessModifier);
	}
}
