package ru.urfu.autoaccessors.annotations;

import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;

class DefaultAnnotationToFieldsMapper extends AnnotationToFieldsMapperBase {

	@Override
	public Set<VariableElement> map(Set<? extends Element> annotatedElements) {
		return ElementFilter.fieldsIn(annotatedElements);
	}
}