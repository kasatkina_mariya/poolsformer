package ru.urfu.autoaccessors.processor.model;
import freemarker.template.DefaultObjectWrapper;
import ru.urfu.autoaccessors.processor.Freemarker;
import ru.urfu.autoaccessors.processor.Utils;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

public class AccessMethodDescription extends DefaultObjectWrapper {

	public final String viewFieldName;
	public final String fullTypeName;
	public final Modifier accessModifier;
	public final String internalFieldName;
	
	private final TypeElement _originalAnnotation;
	
	public AccessMethodDescription(VariableElement fieldElement, TypeElement originalAnnotation) {
		
		super(Freemarker.VERSION);
		
		internalFieldName = fieldElement.getSimpleName().toString();		
		fullTypeName = fieldElement.asType().toString();
		accessModifier = Utils.extractAccessModifier(fieldElement.getModifiers());

		viewFieldName = (internalFieldName.startsWith("_"))
						? internalFieldName.substring(1, 2).toUpperCase() + internalFieldName.substring(2)
						: internalFieldName.substring(0, 1).toUpperCase() + internalFieldName.substring(1);
						
		_originalAnnotation = originalAnnotation;
	}
	
	public TypeElement getOriginalAnnotation() {
		return _originalAnnotation;
	}

	@Override
	public String toString() {
		return String.format("n='%s' vn='%s' t='%s' m='%s'",
							  internalFieldName, viewFieldName, fullTypeName, accessModifier);
	}
}