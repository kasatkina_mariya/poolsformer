package ru.urfu.autoaccessors.processor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.*;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.SourceVersion;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import freemarker.template.TemplateException;
import ru.urfu.autoaccessors.annotations.AnnotationToFieldsMapperBase;
import ru.urfu.autoaccessors.processor.model.ClassDescription;
import ru.urfu.autoaccessors.processor.model.ClassFieldsAccessors;
import ru.urfu.autoaccessors.processor.model.AccessMethodDescription;

@SupportedAnnotationTypes({ "ru.urfu.autoaccessors.annotations.Getter",
							"ru.urfu.autoaccessors.annotations.Setter",
							"ru.urfu.autoaccessors.annotations.pojo.Pojo" })
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class AutoaccessorsProcessor extends AbstractProcessor {

	private final String _TEMPLATE_FILENAME_WITH_ACCESSORS_DECLARATION = "accessorsDeclarationTemplate.txt";
	private final String _TEMPLATE_FILENAME_WITH_ACCESSORS_IMPLEMENTATION = "accessorsImplementationTemplate.txt";
	private final String _CLASSNAME_SUFFIX_FOR_ACCESSORS_DECLARATION = "AccessorsDeclaration";
	private final String _CLASSNAME_SUFFIX_FOR_ACCESSORS_IMPLEMENTATION = "AccessorsImplementation";

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {

		log(new Date().toString() + " " + String.valueOf(annotations.size()));

		if (annotations.size() > 0) {

			Path tempPathToTemplateFile = null;

			try {
				log("started");

				Map<ClassDescription, ClassFieldsAccessors> classesToAccessors = mapClassToAccessMethods(annotations, env);

				tempPathToTemplateFile = Utils.extractResourceFileToTempDirectory(_TEMPLATE_FILENAME_WITH_ACCESSORS_DECLARATION);
				generateSourceFiles(tempPathToTemplateFile, _CLASSNAME_SUFFIX_FOR_ACCESSORS_DECLARATION, classesToAccessors);
				Files.delete(tempPathToTemplateFile);

				tempPathToTemplateFile = Utils.extractResourceFileToTempDirectory(_TEMPLATE_FILENAME_WITH_ACCESSORS_IMPLEMENTATION);
				generateSourceFiles(tempPathToTemplateFile, _CLASSNAME_SUFFIX_FOR_ACCESSORS_IMPLEMENTATION, classesToAccessors);
				Files.delete(tempPathToTemplateFile);

				log("finished");
			} catch (Exception e) {
				log("exception occured");
				log(e.toString(), Diagnostic.Kind.ERROR);
			}
		}

		return true;
	}

	private Map<ClassDescription, ClassFieldsAccessors> mapClassToAccessMethods(Set<? extends TypeElement> annotations, RoundEnvironment env)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Map<ClassDescription, ClassFieldsAccessors> classToAccessors = new HashMap<ClassDescription, ClassFieldsAccessors>();

		for (TypeElement curAnnotation : annotations) {

			log("annotation: " + curAnnotation);
			Set<VariableElement> fieldsToAccess = AnnotationToFieldsMapperBase.createFor(curAnnotation).map(env.getElementsAnnotatedWith(curAnnotation));
			log("fieldsCount: " + fieldsToAccess.size());

			for (VariableElement curField : fieldsToAccess) {

				ClassDescription originalClass = new ClassDescription((TypeElement) curField.getEnclosingElement());
				AccessMethodDescription accessMethod = new AccessMethodDescription(curField, curAnnotation);

				ClassFieldsAccessors accessors;
				if (classToAccessors.containsKey(originalClass))
					accessors = classToAccessors.get(originalClass);
				else {
					accessors = new ClassFieldsAccessors();
					classToAccessors.put(originalClass, accessors);
				}

				accessors.add(accessMethod);
			}
		}

		log("model builded");
		return classToAccessors;
	}

	private void generateSourceFiles(Path pathToTemplateFile, String newClassnameSuffix, Map<ClassDescription, ClassFieldsAccessors> originalClassesToAccessors)
			throws IOException, TemplateException {
		
		String content = new String(Files.readAllBytes(pathToTemplateFile));
		log("template content: " + content);

		for (Entry<ClassDescription, ClassFieldsAccessors> originalClassToAccessors : originalClassesToAccessors.entrySet()) {
			@SuppressWarnings({ "rawtypes", "serial", "unchecked" })
			Map generatedClassModel = new HashMap() {
				{
					put("originalClass", originalClassToAccessors.getKey());
					put("newClassSuffix", newClassnameSuffix);
					put("getters", originalClassToAccessors.getValue().getGetters());
					put("setters", originalClassToAccessors.getValue().getSetters());
				}
			};

			String packageName = originalClassToAccessors.getKey().fullPackageName;
			String className = originalClassToAccessors.getKey().simpleName + newClassnameSuffix;
			String pathToGeneratedSourceFile = packageName.replaceAll("\\.", "/") + "/" + className;
			JavaFileObject javaOutputObject = processingEnv.getFiler().createSourceFile(pathToGeneratedSourceFile);

			Freemarker.applyTemplate(pathToTemplateFile, generatedClassModel, javaOutputObject);
		}
	}

	private void log(String message) {
		log(message, Diagnostic.Kind.NOTE);
	}

	private void log(String message, Diagnostic.Kind kind) {
		processingEnv.getMessager().printMessage(kind, message);
	}
}
