package ru.urfu.autoaccessors.processor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import javax.tools.JavaFileObject;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;
import freemarker.template.Version;

public class Freemarker {
	
	public final static Version VERSION = Configuration.VERSION_2_3_22;
	private final static Configuration _FM_CONF = initFreemarker();

	public static void applyTemplate (Path pathToTemplateFile,
									  @SuppressWarnings("rawtypes") Map model,
									  JavaFileObject outputObject)
		throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException
	{		
		Template fmTemplate = _FM_CONF.getTemplate(pathToTemplateFile.getFileName().toString());		

		try (BufferedWriter bw = new BufferedWriter(outputObject.openWriter())) {
			fmTemplate.process(model, bw);
		}
	}
	
	private static Configuration initFreemarker() {

		Configuration conf = null;

		try {
			conf = new Configuration(VERSION);
			conf.setDefaultEncoding("UTF-8");
			conf.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

			Path pathToTempDirectory = Paths.get(System.getProperty("java.io.tmpdir"));
			File templatesDirectory = pathToTempDirectory.toFile();
			conf.setDirectoryForTemplateLoading(templatesDirectory);
			
			DefaultObjectWrapper wrapper = new DefaultObjectWrapper(VERSION);
			wrapper.setExposeFields(true);
			conf.setObjectWrapper(wrapper);
		} catch (Exception e) {
			throw new RuntimeException("Failed to create Freemarker configuration in static block", e);
		}

		return conf;
	}
}
