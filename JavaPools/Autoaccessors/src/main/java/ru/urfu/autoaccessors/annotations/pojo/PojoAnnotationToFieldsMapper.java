package ru.urfu.autoaccessors.annotations.pojo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;

import ru.urfu.autoaccessors.annotations.AnnotationToFieldsMapperBase;

public class PojoAnnotationToFieldsMapper extends AnnotationToFieldsMapperBase {

	@Override
	public Set<VariableElement> map(Set<? extends Element> annotatedElements) {

		Set<VariableElement> fieldsAccumulator = new HashSet<>();

		for (Element annotatedClass : ElementFilter.typesIn(annotatedElements)) {
			List<? extends Element> enclosedElements = annotatedClass.getEnclosedElements();
			List<VariableElement> fields = ElementFilter.fieldsIn(enclosedElements);
			fieldsAccumulator.addAll(fields);
		}

		return fieldsAccumulator;
	}
}