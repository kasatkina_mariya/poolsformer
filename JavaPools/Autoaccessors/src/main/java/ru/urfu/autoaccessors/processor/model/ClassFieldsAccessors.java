package ru.urfu.autoaccessors.processor.model;

import java.util.LinkedList;
import java.util.List;

import ru.urfu.autoaccessors.processor.Freemarker;
import freemarker.template.DefaultObjectWrapper;

public class ClassFieldsAccessors extends DefaultObjectWrapper {

	private final List<AccessMethodDescription> _getters = new LinkedList<AccessMethodDescription>();;
	private final List<AccessMethodDescription> _setters = new LinkedList<AccessMethodDescription>();;
	
	public ClassFieldsAccessors () {		
		super(Freemarker.VERSION);
	}
	
	public void add (AccessMethodDescription accessMethod) {
		
		String annotationStr = accessMethod.getOriginalAnnotation().toString();
		
		if (annotationStr.contains("Getter"))
			_getters.add(accessMethod);
		else if (annotationStr.contains("Setter"))
			_setters.add(accessMethod);
		else if (annotationStr.contains("Pojo")) {
			_getters.add(accessMethod);
			_setters.add(accessMethod);
		}
	}
	
	public List<AccessMethodDescription> getGetters () {
		return _getters;
	}
	
	public List<AccessMethodDescription> getSetters () {
		return _setters;
	}
}