package ru.urfu.autoaccessors.processor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import javax.lang.model.element.Modifier;

public class Utils {
	
	private static final String _TEMP_FILENAME_PREFIX = "aaaa";
	private static final String _TEMP_FILENAME_SUFFIX = "bbbb";

	public static Path extractResourceFileToTempDirectory(String filename)
		throws IOException {

		// App.class.getClassLoader().getResource("template.txt");
		// URL sourceStream1 = App.class.getResource("/template.txt");

		URL urlToOriginalFile = Utils.class.getResource("/" + filename);
		JarURLConnection connectionToOriginalFile = (JarURLConnection) urlToOriginalFile.openConnection();

		File tempFile = File.createTempFile(_TEMP_FILENAME_PREFIX, _TEMP_FILENAME_SUFFIX);
		tempFile.deleteOnExit();
		Path pathToTargetFile = Paths.get(tempFile.getAbsolutePath());
		tempFile.delete();

		try (InputStream originalFileStream = connectionToOriginalFile.getInputStream();) {
			Files.copy(originalFileStream, pathToTargetFile);
		}
		
		return pathToTargetFile;
	}
	
	public static Modifier extractAccessModifier (Set<Modifier> modifiers) {
		return Modifier.PUBLIC;
	}
}
